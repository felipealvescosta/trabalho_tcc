      $(document).ready(function(){
        //Inicialização dos elementos no JS
        //Parallax BG
        $('.parallax').parallax();

        //sideNav
        $(".button-collapse").sideNav();

        //Tooltips
        $('.tooltipped').tooltip({delay: 50});

        //select_initializer
        $('select').material_select();

        //datepicker
        $('.datepicker').pickadate({
          selectMonths: true, // Creates a dropdown to control month
          selectYears: 15, // Creates a dropdown of 15 years to control year
          //Persnonalizando nomes dos meses do ano
          monthsFull: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
          //Persnonalizando nomes dos dias da semana
          weekdaysFull: [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado' ],
          weekdaysShort: [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab' ],
          weekdaysLetter: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
          today: 'Hoje',
          clear: 'Limpar',
          close: 'Fechar',
          formatSubmit : 'dd/mm/yyyy',
		  format : 'dd/mm/yyyy',
        });

      });