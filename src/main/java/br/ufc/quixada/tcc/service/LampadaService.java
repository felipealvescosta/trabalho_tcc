package br.ufc.quixada.tcc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.quixada.tcc.model.Lampada;
import br.ufc.quixada.tcc.repository.LampadaRepository;

@Service
public class LampadaService {
	
	@Autowired
	private LampadaRepository lampadaRepository;
	
	public void saveOrEdit(Lampada lampada){
		lampadaRepository.save(lampada);
	}
	
	public Lampada getLampada(Long idLampada){
		return lampadaRepository.findLampadaById(idLampada);
	}
}