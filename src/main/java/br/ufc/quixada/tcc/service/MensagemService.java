package br.ufc.quixada.tcc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

@Service
public class MensagemService {
	@Autowired
	private MessageSource message;
	
	public String getMessage(String identificador) throws NoSuchMessageException{
		return message.getMessage(identificador, null, null);
	}
}
