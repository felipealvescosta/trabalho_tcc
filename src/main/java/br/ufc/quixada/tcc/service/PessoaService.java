package br.ufc.quixada.tcc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.ufc.quixada.tcc.model.Pessoa;
import br.ufc.quixada.tcc.repository.PessoaRepository;

@Service
public class PessoaService implements UserDetailsService{
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Pessoa pessoa = pessoaRepository.findByEmail(email);
		
		if(pessoa == null){
			throw new UsernameNotFoundException("Pessoa não econtrada");
		}
		
		return pessoa;
	}
	
	public void saveOrEdit(Pessoa pessoa){
		pessoaRepository.save(pessoa);
	}

}