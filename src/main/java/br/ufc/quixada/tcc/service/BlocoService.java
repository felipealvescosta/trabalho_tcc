package br.ufc.quixada.tcc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.quixada.tcc.model.Bloco;
import br.ufc.quixada.tcc.repository.BlocoRepository;

@Service
public class BlocoService {
	@Autowired
	private BlocoRepository blocoRepository;
	
	public List<Bloco> getAllBlocos(){
		return (List<Bloco>) blocoRepository.findAll();
	}
	
	public void saveOrEdit(Bloco bloco){
		blocoRepository.save(bloco);
	}
	
	public Bloco getBloco(Long id){
		return blocoRepository.findBlocoById(id);
	}
	
	public void excluir(Long idBloco){
		blocoRepository.delete(idBloco);
	}
	
	public boolean exiteBloco(Long id){
		return blocoRepository.exists(id);
	}
}