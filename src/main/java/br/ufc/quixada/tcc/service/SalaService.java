package br.ufc.quixada.tcc.service;

import java.sql.Time;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.quixada.tcc.model.Sala;
import br.ufc.quixada.tcc.repository.SalaRepository;

@Service
public class SalaService {
	
	@Autowired
	private SalaRepository salaRepository;
	
	public List<Sala> getSalasDoBloco(Long idBloco){
		return salaRepository.findSalaByBlocoId(idBloco);
	}
	
	public void saveOrEdit(Sala sala){
		salaRepository.save(sala);
	}
	
	public void excluir(Long idSala){
		salaRepository.delete(idSala);
	}
	
	public Sala getSala(Long idSala){
		return salaRepository.findSalaById(idSala);
	}
	
	public List<Sala> getAll(){
		return (List<Sala>) salaRepository.findAll();
	}
	
	public Sala verificaSeExisteSalaComNomeNoBloco(Long idBloco, String nomeSala){
		return salaRepository.findSalaByBlocoIdAndNome(idBloco, nomeSala);
	}
	
	public List<Sala> getSalasParaLigarDeManha(Time horario){
		return salaRepository.findSalaByManhaLigar(horario);
	}
	
	public List<Sala> getSalasParaLigarDeTarde(Time horario){
		return salaRepository.findSalaByTardeLigar(horario);
	}
	
	public List<Sala> getSalasParaLigarDeNoite(Time horario){
		return salaRepository.findSalaByNoiteLigar(horario);
	}
	
	public List<Sala> getSalasParaDesligarDeManha(Time horario){
		return salaRepository.findSalaByManhaDesligar(horario);
	}
	
	public List<Sala> getSalasParaDesligarDeTarde(Time horario){
		return salaRepository.findSalaByTardeDesligar(horario);
	}
	
	public List<Sala> getSalasParaDesligarDeNoite(Time horario){
		return salaRepository.findSalaByNoiteDesligar(horario);
	}
}