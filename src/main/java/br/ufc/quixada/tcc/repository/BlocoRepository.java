package br.ufc.quixada.tcc.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.ufc.quixada.tcc.model.Bloco;

@Repository
@Transactional
public interface BlocoRepository extends CrudRepository<Bloco, Long>{
	public Bloco findBlocoById(Long id);
}