package br.ufc.quixada.tcc.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.ufc.quixada.tcc.model.ArCondicionado;

@Repository
@Transactional
public interface ArCondicionadoRepository extends CrudRepository<ArCondicionado, Long>{
	public ArCondicionado findByIdOrderById(Long idArCondicionado);
}