package br.ufc.quixada.tcc.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.ufc.quixada.tcc.model.Projetor;

@Repository
@Transactional
public interface ProjetorRepository extends CrudRepository<Projetor, Long>{
	public Projetor findProjetorById(Long idProjetor);
}