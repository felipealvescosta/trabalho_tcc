package br.ufc.quixada.tcc.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.ufc.quixada.tcc.model.Lampada;

@Repository
@Transactional
public interface LampadaRepository extends CrudRepository<Lampada, Long>{
	public Lampada findLampadaById(Long idLampada);
}