package br.ufc.quixada.tcc.model;

import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "sala")
public class Sala {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "nome", nullable = false)
	@NotEmpty
	private String nome;
	
	@Column(name = "local")
	private String local;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Bloco bloco;
	
	@OneToMany(mappedBy = "sala", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Lampada> lampadas;
	
	@OneToMany(mappedBy = "sala", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ArCondicionado> arCondicionados;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Projetor projetor;
	
	@Column(name = "manha_ligar")
	private Time manhaLigar;
	
	@Column(name = "manha_desligar")
	private Time manhaDesligar;
	
	@Column(name = "tarde_ligar")
	private Time tardeLigar;
	
	@Column(name = "tarde_desligar")
	private Time tardeDesligar;
	
	@Column(name = "noite_ligar")
	private Time noiteLigar;
	
	@Column(name = "noite_desligar")
	private Time noiteDesligar;
	


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public Bloco getBloco() {
		return bloco;
	}

	public void setBloco(Bloco bloco) {
		this.bloco = bloco;
	}

	public List<Lampada> getLampadas() {
		return lampadas;
	}

	public void setLampadas(List<Lampada> lampadas) {
		this.lampadas = lampadas;
	}

	public List<ArCondicionado> getArCondicionados() {
		return arCondicionados;
	}

	public void setArCondicionados(List<ArCondicionado> arCondicionados) {
		this.arCondicionados = arCondicionados;
	}

	public Projetor getProjetor() {
		return projetor;
	}

	public void setProjetor(Projetor projetor) {
		this.projetor = projetor;
	}

	public Time getManhaLigar() {
		return manhaLigar;
	}

	public void setManhaLigar(Time manhaLigar) {
		this.manhaLigar = manhaLigar;
	}

	public Time getManhaDesligar() {
		return manhaDesligar;
	}

	public void setManhaDesligar(Time manhaDesligar) {
		this.manhaDesligar = manhaDesligar;
	}

	public Time getTardeLigar() {
		return tardeLigar;
	}

	public void setTardeLigar(Time tardeLigar) {
		this.tardeLigar = tardeLigar;
	}

	public Time getTardeDesligar() {
		return tardeDesligar;
	}

	public void setTardeDesligar(Time tardeDesligar) {
		this.tardeDesligar = tardeDesligar;
	}

	public Time getNoiteLigar() {
		return noiteLigar;
	}

	public void setNoiteLigar(Time noiteLigar) {
		this.noiteLigar = noiteLigar;
	}

	public Time getNoiteDesligar() {
		return noiteDesligar;
	}

	public void setNoiteDesligar(Time noiteDesligar) {
		this.noiteDesligar = noiteDesligar;
	}


	@Override
	public String toString() {
		return "Sala [id=" + id + ", nome=" + nome + ", local=" + local + ", bloco=" + bloco + ", lampadas=" + lampadas
				+ ", arCondicionados=" + arCondicionados + ", projetor=" + projetor + ", manhaLigar=" + manhaLigar
				+ ", manhaDesligar=" + manhaDesligar + ", tardeLigar=" + tardeLigar + ", tardeDesligar=" + tardeDesligar
				+ ", noiteLigar=" + noiteLigar + ", noiteDesligar=" + noiteDesligar + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arCondicionados == null) ? 0 : arCondicionados.hashCode());
		result = prime * result + ((bloco == null) ? 0 : bloco.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lampadas == null) ? 0 : lampadas.hashCode());
		result = prime * result + ((local == null) ? 0 : local.hashCode());
		result = prime * result + ((manhaDesligar == null) ? 0 : manhaDesligar.hashCode());
		result = prime * result + ((manhaLigar == null) ? 0 : manhaLigar.hashCode());
		result = prime * result + ((noiteDesligar == null) ? 0 : noiteDesligar.hashCode());
		result = prime * result + ((noiteLigar == null) ? 0 : noiteLigar.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((projetor == null) ? 0 : projetor.hashCode());
		result = prime * result + ((tardeDesligar == null) ? 0 : tardeDesligar.hashCode());
		result = prime * result + ((tardeLigar == null) ? 0 : tardeLigar.hashCode());
		return result;
	}



}