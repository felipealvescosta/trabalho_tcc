package br.ufc.quixada.tcc;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.scheduling.annotation.EnableScheduling;

import br.ufc.quixada.tcc.mqtt.MqttPublish;

@SpringBootApplication
@EnableScheduling
@IntegrationComponentScan
public class ProjetoTccApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoTccApplication.class, args);
	}
	
	@Bean
    public MqttPahoClientFactory mqttClientFactory() {
		String ip = "192.168.129.147";
		String ipUFC = "10.0.121.45";
		DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setServerURIs("tcp://"+ip+":1883");
        //factory.setUserName("username");
        //factory.setPassword("password");
        return factory;
    }
	
	@Bean(name = "mqttPublish")
	public MqttPublish mqttInstanceConection() throws MqttException{
		MqttPublish mqtt = new MqttPublish();
		return mqtt;
	}

    @Bean
    @ServiceActivator(inputChannel = "mqttOutboundChannel")
    public MessageHandler mqttOutbound() {
        MqttPahoMessageHandler messageHandler =
                       new MqttPahoMessageHandler("testClient", mqttClientFactory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic("testTopic");
        return messageHandler;
    }

    @Bean
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

    @MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
    public interface MyGateway {
        void sendToMqtt(String data);
    }
    
    
    @Bean
    public MessageChannel mqttInputChannel() {
    	return new DirectChannel();
    }
    
    @Bean
    public MessageProducer inbound() {
    	MqttPahoMessageDrivenChannelAdapter adapter =
    			new MqttPahoMessageDrivenChannelAdapter("tcp://localhost:1883", "testClient",
    			                                 "topic1", "topic2");
    	adapter.setCompletionTimeout(5000);
    	adapter.setConverter(new DefaultPahoMessageConverter());
    	adapter.setQos(1);
    	adapter.setOutputChannel(mqttInputChannel());
    	return adapter;
    }
    
    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
    	return new MessageHandler() {

    		@Override
    		public void handleMessage(Message<?> message) throws MessagingException {
    			System.out.println(message.getPayload());
    		}

    	};
    }
}
