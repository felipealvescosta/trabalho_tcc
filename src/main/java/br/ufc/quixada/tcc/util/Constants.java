package br.ufc.quixada.tcc.util;

import org.springframework.context.annotation.Configuration;

@Configuration
public class Constants {
	public static final String CADASTRAR_BLOCO = "views/cadastrar_bloco";
	public static final String EDITAR_BLOCO = "views/editar_bloco";
	public static final String CADASTRAR_SALA =  "views/cadastrar_sala";
	public static final String DETALHES_SALA = "views/detalhes_sala";
	public static final String EDITAR_OBJETOS = "views/editar_objetos";
	public static final String EDITAR_SALA = "views/editar_sala";
	public static final String VER_SALA = "views/ver_salas";
	public static final String INDEX = "views/index";
	public static final String VER_ESTADO = "views/ver_estado";
	public static final String ATRIBUIR_ARDUINO = "views/atribuir_arduino";
	public static final String CADASTRAR_ARDUINO = "views/cadastrar_arduino";
	
	public static final String LIGADA = "LIGADA";
	public static final String DESLIGADA = "DESLIGADA";
	
	public static final String LIGAR = "1";
	public static final String DESLIGAR = "2";
}