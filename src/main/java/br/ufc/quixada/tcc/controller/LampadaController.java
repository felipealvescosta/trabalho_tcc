package br.ufc.quixada.tcc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.quixada.tcc.model.Lampada;
import br.ufc.quixada.tcc.mqtt.MqttPublish;
import br.ufc.quixada.tcc.service.LampadaService;
import br.ufc.quixada.tcc.service.MensagemService;
import br.ufc.quixada.tcc.util.Constants;

@Controller
@RequestMapping(value = {"/lampada"})
public class LampadaController {
	
	private static final String LAMPADA_LIGADA = "LAMPADA_LIGADA";

	private static final String LAMPADA_DESLIGADA = "LAMPADA_DESLIGADA";

	@Autowired
	private LampadaService lampadaService;
	
	@Autowired
	private MqttPublish pubAndSub;
	
	@Autowired
	private MensagemService mensagemService;
	
	@RequestMapping(value = {"/ligar_desligar_lampada/{idLampada}/{idSala}"}, method = RequestMethod.GET)
	public String ligarObjeto(@PathVariable String idLampada, @PathVariable String idSala, RedirectAttributes redirect){
		
		Long idLamp = Long.parseLong(idLampada);
		Lampada lampada = lampadaService.getLampada(idLamp);
		
		if(lampada.isEstado()){
			lampada.setEstado(false);
			lampadaService.saveOrEdit(lampada);
			pubAndSub.publicarTopico(Constants.DESLIGAR, lampada.getLocal());
			redirect.addFlashAttribute("desligado", mensagemService.getMessage(LAMPADA_DESLIGADA));
		}else{
			lampada.setEstado(true);
			lampadaService.saveOrEdit(lampada);
			pubAndSub.publicarTopico(Constants.LIGAR, lampada.getLocal());
			redirect.addFlashAttribute("ligado", mensagemService.getMessage(LAMPADA_LIGADA));
		}

		return "redirect:/sala/detalhe_sala/"+idSala;
	}
}