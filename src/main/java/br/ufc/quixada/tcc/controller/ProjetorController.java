package br.ufc.quixada.tcc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.quixada.tcc.model.Projetor;
import br.ufc.quixada.tcc.mqtt.MqttPublish;
import br.ufc.quixada.tcc.service.MensagemService;
import br.ufc.quixada.tcc.service.ProjetorService;
import br.ufc.quixada.tcc.util.Constants;

@Controller
@RequestMapping(value = {"/projetor"})
public class ProjetorController {
	
	private static final String PROJETOR_LIGADO = "PROJETOR_LIGADO";

	private static final String PROJETOR_DESLIGADO = "PROJETOR_DESLIGADO";

	@Autowired
	private ProjetorService projetorService;
	
	@Autowired
	private MqttPublish pubAndSub;
	
	@Autowired
	private MensagemService mensagemService;
	
	@RequestMapping(value = {"/ligar_desligar_projetor/{idProjetor}/{idSala}"}, method = RequestMethod.GET)
	public String ligarObjeto(@PathVariable String idProjetor, @PathVariable String idSala, RedirectAttributes redirect){
		
		Long idProj = Long.parseLong(idProjetor);
		Projetor projetor = projetorService.getProjetor(idProj);
		
		if(projetor.isEstado()){
			projetor.setEstado(false);
			projetorService.saveOrEdit(projetor);
			
			pubAndSub.publicarTopico(Constants.DESLIGAR, projetor.getLocal());
			redirect.addFlashAttribute("desligado", mensagemService.getMessage(PROJETOR_DESLIGADO));
		}else{
			projetor.setEstado(true);
			projetorService.saveOrEdit(projetor);
			pubAndSub.publicarTopico(Constants.LIGAR, projetor.getLocal());
			redirect.addFlashAttribute("ligado", mensagemService.getMessage(PROJETOR_LIGADO));
		}

		return "redirect:/sala/detalhe_sala/"+idSala;
	}
}