package br.ufc.quixada.tcc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.quixada.tcc.model.Bloco;
import br.ufc.quixada.tcc.service.BlocoService;
import br.ufc.quixada.tcc.service.MensagemService;
import br.ufc.quixada.tcc.util.Constants;

@Controller
@RequestMapping(value = {"/","/bloco"})
public class BlocoController {
	private static final String BLOCO_EXCLUIDO = "BLOCO_EXCLUIDO";

	private static final String PATH_UFC_CAMPUS_QXD = "UFC/CAMPUS_QXD/";

	private static final String BLOCO_CADASTRADO = "BLOCO_CADASTRADO";

	@Autowired
	private BlocoService blocoService;
	
	@Autowired 
	private MensagemService message;
	
	@RequestMapping(value = {"/","/bloco"}, method = RequestMethod.GET)
	public String mostrarBlocos(Model model){
		List<Bloco> blocos = blocoService.getAllBlocos();
		model.addAttribute("blocos", blocos);
		return Constants.INDEX;
	}
	
	@RequestMapping(value = {"/cadastrar"}, method = RequestMethod.GET)
	public String cadastrarBloco(Model model){
		model.addAttribute("bloco", new Bloco());
		return Constants.CADASTRAR_BLOCO;
	}
	
	@RequestMapping(value = {"/cadastrar"}, method = RequestMethod.POST)
	public String cadastrarBloco(@Valid Bloco bloco, BindingResult result, RedirectAttributes redirect){
		StringBuilder path = new StringBuilder();
		path.append(PATH_UFC_CAMPUS_QXD);
		path.append(bloco.getNome().trim().replace(" ", "_"));
		
		bloco.setLocal(path.toString());
		blocoService.saveOrEdit(bloco);
		redirect.addFlashAttribute("cadastrado", message.getMessage(BLOCO_CADASTRADO));
		return "redirect:/bloco";
	}
	
	@RequestMapping(value = {"/editar/{idBloco}"}, method = RequestMethod.GET)
	public String editar(@PathVariable("idBloco") String idBloco, Model model){
		
		model.addAttribute("bloco", blocoService.getBloco(Long.parseLong(idBloco)));
		
		return Constants.EDITAR_BLOCO;
	}
	
	@RequestMapping(value = {"/editar"}, method = RequestMethod.POST)
	public String editar(@Valid Bloco bloco, BindingResult result, RedirectAttributes redirect){
		
		if(blocoService.exiteBloco(bloco.getId())){
			blocoService.saveOrEdit(bloco);
		}
		
		return "redirect:/";
	}
	
	
	@RequestMapping(value = {"/excluir"}, method = RequestMethod.POST)
	public String excluir(@RequestParam("idBloco") String idBloco, RedirectAttributes redirect){
		
		blocoService.excluir(Long.parseLong(idBloco));
		
		redirect.addFlashAttribute("excluido", message.getMessage(BLOCO_EXCLUIDO));
		return "redirect:/";
	}
}
