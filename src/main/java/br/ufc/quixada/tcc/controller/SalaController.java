package br.ufc.quixada.tcc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.quixada.tcc.model.ArCondicionado;
import br.ufc.quixada.tcc.model.Bloco;
import br.ufc.quixada.tcc.model.Lampada;
import br.ufc.quixada.tcc.model.Projetor;
import br.ufc.quixada.tcc.model.Sala;
import br.ufc.quixada.tcc.mqtt.MqttPublish;
import br.ufc.quixada.tcc.service.ArCondicionadoService;
import br.ufc.quixada.tcc.service.BlocoService;
import br.ufc.quixada.tcc.service.LampadaService;
import br.ufc.quixada.tcc.service.MensagemService;
import br.ufc.quixada.tcc.service.ProjetorService;
import br.ufc.quixada.tcc.service.SalaService;
import br.ufc.quixada.tcc.util.Constants;

@Controller
@RequestMapping(value = {"/sala"})
public class SalaController {
	private static final String NOME_LAMPADA = "lampada";

	private static final String NOME_ARCONDICIONADO = "arcondicionado";

	private static final String NOME_SALA_EXITE_BLOCO = "NOME_SALA_EXITE_BLOCO";

	private static final String SALA_CADASTRADA = "SALA_CADASTRADA";

	@Autowired
	private BlocoService blocoService;
	
	@Autowired
	private SalaService salaService;
	
	@Autowired
	private ArCondicionadoService arCondicionadoService;
	
	@Autowired 
	private LampadaService lampadaService;
	
	@Autowired
	private ProjetorService projetorService;
	
	@Autowired
	private MqttPublish pubAndSub;
	
	@Autowired
	private MensagemService mensagemService;
	
	@RequestMapping(value = {"/listar/{idBloco}"}, method = RequestMethod.GET)
	public String listarSalas(@PathVariable String idBloco ,Model model){
		Long id = Long.parseLong(idBloco);
		
		Bloco bloco = blocoService.getBloco(id);
		List<Sala> salas = salaService.getSalasDoBloco(id);
		
		model.addAttribute("bloco", bloco);
		model.addAttribute("salas", salas);
		return Constants.VER_SALA;
	}
	
	@RequestMapping(value = {"/cadastrar"}, method = RequestMethod.GET)
	public String cadastrarSala(Model model){
		List<Bloco> blocos = blocoService.getAllBlocos();
		
		model.addAttribute("blocos", blocos);
		model.addAttribute("sala", new Sala());
		return Constants.CADASTRAR_SALA;
	}
	
	@RequestMapping(value = {"/cadastrar"}, method = RequestMethod.POST)
	public String cadastrarSala(@RequestParam("qtd_ar_condicionados") String qtdArCondicionados,
			@RequestParam("qtd_lampadas") String qtdLampadas, @RequestParam("bloco") String id, 
			@Valid Sala sala, BindingResult result, RedirectAttributes redirect){
		
		Long idBloco = Long.parseLong(id);
		Long numeroArCond = Long.parseLong(qtdArCondicionados);
		Long numeroLampadas = Long.parseLong(qtdLampadas);
		Sala s = salaService.verificaSeExisteSalaComNomeNoBloco(idBloco, sala.getNome().trim());
		if(s == null){
			Bloco bloco = blocoService.getBloco(idBloco);
			
			String localSala = path(bloco.getLocal(), sala.getNome());
			
			Projetor projetor = new Projetor();
			addProjetor(localSala, projetor);
			
			addArcondicionados(numeroArCond, localSala, sala);
			
			sala.setLocal(localSala);
			sala.setProjetor(projetor);
			sala.setBloco(bloco);
			
			addLampadas(numeroLampadas, localSala, sala);
			
			pubAndSub.subscribingTopic(localSala);
			pubAndSub.subscribingTopic(projetor.getLocal());
			
			redirect.addFlashAttribute("cadastrado", mensagemService.getMessage(SALA_CADASTRADA));
			
			return "redirect:/sala/listar/"+idBloco;
		}
		
		redirect.addFlashAttribute("erro", mensagemService.getMessage(NOME_SALA_EXITE_BLOCO));
		
		return "redirect:/sala/cadastrar";
	}
	
	@RequestMapping(value = {"/excluir"}, method = RequestMethod.POST)
	public String excluir(@RequestParam("idSala") String idSala, @RequestParam("idBloco") String idBloco, Model model){
		
		salaService.excluir(Long.parseLong(idSala));

		return "redirect:/sala/listar/"+idBloco;
	}
	
	@RequestMapping(value = {"/detalhe_sala/{idSala}"}, method = RequestMethod.GET)
	public String detalhesSala(@PathVariable String idSala, Model model){
		Long id = Long.parseLong(idSala);
		Sala sala = salaService.getSala(id);
		ArCondicionado arCondicionado = new ArCondicionado();
		model.addAttribute("sala", sala);
		model.addAttribute("condicionadorDeAr", arCondicionado);
		return Constants.DETALHES_SALA;
	}
	
	@RequestMapping(value = {"/editar_objetos/{idSala}"}, method = RequestMethod.GET)
	public String editarObjetos(@PathVariable String idSala, Model model){
		Long id = Long.parseLong(idSala);
		Sala sala = salaService.getSala(id);
		model.addAttribute("sala", sala);
		return Constants.EDITAR_OBJETOS;
	}
	
	@RequestMapping(value = {"/configurar"}, method = RequestMethod.POST)
	public String configurarSala(@Valid Sala sala, BindingResult result, Model model){
		
		Sala salaBanco = salaService.getSala(sala.getId());
		
		salaBanco.setManhaLigar(sala.getManhaLigar());
		salaBanco.setManhaDesligar(sala.getManhaDesligar());
		
		salaBanco.setTardeLigar(sala.getTardeLigar());
		salaBanco.setTardeDesligar(sala.getTardeDesligar());
		
		salaBanco.setNoiteLigar(sala.getNoiteLigar());
		salaBanco.setNoiteDesligar(sala.getNoiteDesligar());


		salaService.saveOrEdit(salaBanco);
		
		return "redirect:/sala/detalhe_sala/"+ sala.getId();
	}
	
	@RequestMapping(value = {"/editar/{idSala}"}, method = RequestMethod.GET)
	public String editarSala(@PathVariable("idSala") String idSala, Model model){
		model.addAttribute("sala", salaService.getSala(Long.parseLong(idSala)));
		
		return Constants.EDITAR_SALA;
	}
	
	@RequestMapping(value = {"/editar"}, method = RequestMethod.POST)
	public String editarSala( @Valid Sala sala, 
			Model model){

		Sala s = salaService.getSala(sala.getId());

		s.setNome(sala.getNome().trim());
		salaService.saveOrEdit(s);
		
		return "redirect:/";
	}
	
	
	public void addArcondicionados(Long numeroArCond, String localSala, Sala sala){
		String localArcondiconado;
		for(int i = 0; i < numeroArCond; i++){
			ArCondicionado arCondicionado = new ArCondicionado();
			localArcondiconado = path(localSala, NOME_ARCONDICIONADO+i); 
			arCondicionado.setLocal(localArcondiconado);
			arCondicionado.setSala(sala);
			arCondicionado.setEstado(false);
			arCondicionado.setNome("Ar Condicionado " + i);
			arCondicionado.setTemperatura(18L);
			arCondicionadoService.saveOrEdit(arCondicionado);
			pubAndSub.subscribingTopic(localArcondiconado);
		}
	}
	
	public void addLampadas(Long numeroLampadas, String localSala, Sala sala){
		String localLampada;
		for(int i = 0; i < numeroLampadas; i++){
			Lampada lampada = new Lampada();
			localLampada = path(localSala, NOME_LAMPADA+i); 
			lampada.setLocal(localLampada);
			lampada.setSala(sala);
			lampada.setEstado(false);
			lampadaService.saveOrEdit(lampada);
			pubAndSub.subscribingTopic(localLampada);
		}
	}
	
	public void addProjetor(String localSala, Projetor projetor){
		String nomeProjetor = "projetor";
		String localProjetor = path(localSala, nomeProjetor);
		projetor.setNome("projeto/" + localSala);
		projetor.setLocal(localProjetor);
		projetor.setEstado(false);
		
		projetorService.saveOrEdit(projetor);
	}
	
	public String path(String caminhoPai, String nomeObjeto){
		StringBuilder caminhoDefinitivo = new StringBuilder();
		caminhoDefinitivo.append(caminhoPai);
		caminhoDefinitivo.append("/");
		caminhoDefinitivo.append(nomeObjeto.trim().replace(" ", "_"));
		return caminhoDefinitivo.toString();
	}
}
