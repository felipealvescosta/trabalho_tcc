package br.ufc.quixada.tcc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.ufc.quixada.tcc.model.Papel;
import br.ufc.quixada.tcc.model.Pessoa;
import br.ufc.quixada.tcc.service.MensagemService;
import br.ufc.quixada.tcc.service.PessoaService;

@Controller
@RequestMapping("/login")
public class LoginController{
	
	private static final String ERRO_AO_SE_CADASTRAR = "ERRO_AO_SE_CADASTRAR";

	private static final String PESSOA_CADASTRADA_COM_SUCESSO = "PESSOA_CADASTRADA_COM_SUCESSO";

	@Autowired
	private PessoaService pessoaService;
	
	@Autowired
	private MensagemService mensagemService;
	
	@RequestMapping(value = {""}, method = RequestMethod.GET)
	public String login(){
		return "login";
	}
	
	@RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
	public String cadastrarPessoa(Model model){
		model.addAttribute("pessoa", new Pessoa());
		return "cadastrar";
	}
	
	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	public String cadastrarPessoa(@Valid Pessoa pessoa, BindingResult result, Model model){
		if(!result.hasErrors()){
			pessoa.setPapel(Papel.PROFESSOR);
			pessoaService.saveOrEdit(pessoa);
			model.addAttribute("sucesso", mensagemService.getMessage(PESSOA_CADASTRADA_COM_SUCESSO));
			return "login";
		}
		model.addAttribute("erro", mensagemService.getMessage(ERRO_AO_SE_CADASTRAR));
		return "redirect:/login/cadastrar";
	}
}