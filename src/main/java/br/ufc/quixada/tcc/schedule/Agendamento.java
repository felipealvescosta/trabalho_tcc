package br.ufc.quixada.tcc.schedule;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

@Component
public class Agendamento {
    private TaskScheduler scheduler;
    

    @Scheduled(fixedRate = 1000)
    public void reportCurrentTime() {
    	//faz algo
    }
    
    Runnable exampleRunnable = new Runnable(){
        @Override
        public void run() {
            System.out.println("Works");
        }
    };

    @Async
    public void executeTaskT() {
        ScheduledExecutorService localExecutor = Executors.newSingleThreadScheduledExecutor();
        scheduler = new ConcurrentTaskScheduler(localExecutor);
        System.out.println("ENTROU AUQI aushuashuahsuahs");
        scheduler.scheduleWithFixedDelay(exampleRunnable,10L);
    }

    
    
}